'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
