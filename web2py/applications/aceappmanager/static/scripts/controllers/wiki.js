'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:WikiCtrl
 * @description
 * # WikiCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('WikiCtrl', function ($scope, $rootScope, $location, AcePropManagerFactory) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    console.log("inside wiki js");
    $scope.keyStrokes = 0;


    $scope.updateContents = function(){
        $scope.keyStrokes += 1;
        console.log("key strokes " + $scope.keyStrokes);
        if($scope.keyStrokes === 5){
            $scope.keyStrokes = 0;
            AcePropManagerFactory.saveArticle.query($scope.articleNotes, function(data){
                console.log(data);
            }, function(error){
                console.log(error);
            });
        };
    };


  });