'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:AddpropertyCtrl
 * @description
 * # AddpropertyCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('AddPropertyCtrl', function ($scope, $location, $rootScope, $timeout, AcePropManagerFactory) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.releases = [1, 2, 3, 4, 5, 6, 7, 8];
    $scope.sprints = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    $scope.fileNames = [];
    $scope.selectedRelease = "select release";
    $scope.selectedSprint = "Select sprint";
    $scope.selectedFileName = "Select file";
    $scope.propertyDetails = {};

    AcePropManagerFactory.getPropFileNames.query(function(data){
        angular.forEach(data, function(file){
            $scope.fileNames.push(file.filename);
        });
    }, function(error){
        console.log("something went wrong while fetching files list");
    })

    $scope.onSelectRelease = function(release){
        $scope.selectedRelease = release;
        $scope.propertyDetails.release = +release;
    }

    $scope.onSelectSprint = function(sprint){
        $scope.selectedSprint = sprint;
        $scope.propertyDetails.sprint = +sprint;
    }

    $scope.onSelectFileName = function(fileName){
        $scope.selectedFileName = fileName;
        $scope.propertyDetails.fileName = fileName;
    }

    $scope.save = function(propertyDetails){
        console.log("propertyDetails is " + JSON.stringify(propertyDetails));

        AcePropManagerFactory.addProperties.query(propertyDetails,
            function(data){
                console.log('properties list is ' + JSON.stringify(data));
                $location.path('/profile');
                $rootScope.alertMessage = "successfully added";
                $rootScope.showAlert = true;
                $rootScope.alertClass = 'alert alert-dismissable alert-success';
                $timeout($rootScope.hideAlertBox, 4000);
            },
            function(error){
                $rootScope.alertMessage = error.data;
                $rootScope.showAlert = true;
                $rootScope.alertClass = 'alert alert-dismissable alert-danger';
                $timeout($rootScope.hideAlertBox, 4000);
            }
        );

    }
  });

