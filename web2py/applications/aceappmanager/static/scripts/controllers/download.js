'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:DownloadCtrl
 * @description
 * # DownloadCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('DownloadCtrl', function ($scope, $rootScope, $location, AcePropManagerFactory) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];


    $scope.propertyURL = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/" + "aceappmanager/v1/my_small_file_downloader/properties.zip";
    console.log('inside download controller');

    if(angular.isUndefined($rootScope.globalUser) 
        || $rootScope.globalUser === null
        || angular.equals({}, $rootScope.globalUser)){
        console.log($rootScope.globalUser);
        $location.path('/login');
    }
    
    $scope.getHeader = function () {
        return ["property_key", "value_en", "value_th", "value_ar", "value_ru","value_da","value_nl","value_fi","value_fr","value_de","value_el","value_it","value_nb","value_sv","value_tr","file_name"]
    };

    if(angular.isUndefined($rootScope.properties) || $rootScope.properties === null){
        AcePropManagerFactory.getPropertiesListWithDetails.query(
            function(data){
                console.log('properties list is ' + JSON.stringify(data));
                /*for (var i = 0; i < data.length; i++){
                    $scope.editMode[i] = 0;
                }*/
                $rootScope.properties = [];
                $rootScope.propertiesObj = data;
                for(var key in $rootScope.propertiesObj){
                    $rootScope.properties.push($rootScope.propertiesObj[key]);
                }
                console.log($rootScope.properties);
            },
            function(error){
                console.log('Ouch something went wrong ' + error.data);
            }
        );    
    }

    $scope.prepareProperties = function(){
        // Uncomment the lines in this function to have download function work in Safari browser
        var usedProperties = [];
        /*var csvContent = "data:text/csv;charset=utf-8,";*/
        for(var i=0; i<$rootScope.properties.length; i++){
            var property = $rootScope.properties[i];
            var usedProperty = {};
            usedProperty.property_key = property.property_key;
            usedProperty.value_en = property.value_en;
            usedProperty.value_th = property.value_th;
            usedProperty.value_ar = property.value_ar;
            usedProperty.value_ru = property.value_ru;
            usedProperty.value_da = property.value_da;
            usedProperty.value_nl = property.value_nl;
            usedProperty.value_fi = property.value_fi;
            usedProperty.value_fr = property.value_fr;
            usedProperty.value_de = property.value_de;
            usedProperty.value_el = property.value_el;
            usedProperty.value_it = property.value_it;
            usedProperty.value_nb = property.value_nb;
            usedProperty.value_sv = property.value_sv;
            usedProperty.value_tr = property.value_tr;
            usedProperty.file_name = property.file_name;
            usedProperties.push(usedProperty);
            /*var dataString = '"' + property.property_key + '","' + property.value_en + '","' + property.value_th + '","' + property.value_ge + '"' + '\n';
            csvContent += dataString;*/
        }
        /*var encodedUri = encodeURI(csvContent);
        window.open(encodedUri);*/
        return usedProperties;
    }
  });
