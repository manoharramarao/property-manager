'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:ImportCtrl
 * @description
 * # ImportCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('ImportCtrl', function ($scope, $rootScope, $location, $upload) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    var importUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/" + "aceappmanager/user/upload_properties";
    
    $scope.showAlert = false;

    if(angular.isUndefined($rootScope.globalUser) 
        || $rootScope.globalUser === null
        || angular.equals({}, $rootScope.globalUser)){
        console.log($rootScope.globalUser);
        $location.path('/login');
    }

    $scope.onFileSelect = function($files) {
        for (var i = 0; i < $files.length; i++) {
            var $file = $files[i];
            $upload.upload({
                url: importUrl,
                file: $file,
            }).progress(function(evt) {
                console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            }).success(function(data, status, headers, config){
                $scope.showAlert = true;
                $scope.alertMsg = "Successfully imported"
            })
        }
    }
  });
