'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
