'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('LoginCtrl', function ($scope, $location, $rootScope, $timeout, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.user = {}
    $scope.loginError = "";

    if(!angular.isUndefined($rootScope.globalUser) 
        && !angular.equals({}, $rootScope.globalUser)){
      $location.path('/profile');
    }



    $scope.login = function(user){
        UserService.login.query(user, function(data){
            $scope.loginError = "";
            $rootScope.globalUser = data;
            UserService.user = data;
            $location.path('/profile');
        }, function(error){
            $scope.loginError = "wrong credentials";
        });
    }
  });
