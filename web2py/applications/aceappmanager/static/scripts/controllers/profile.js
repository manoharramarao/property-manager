'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:ProfileCtrl
 * @description
 * # ProfileCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('ProfileCtrl', function ($scope, $rootScope, $location) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    if(angular.isUndefined($rootScope.globalUser) 
        || $rootScope.globalUser === null
        || angular.equals({}, $rootScope.globalUser)){
        console.log($rootScope.globalUser);
        $location.path('/login');
    }

  });
