'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:GlobalCtrl
 * @description
 * # GlobalCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('GlobalCtrl', function ($scope, $location, $rootScope, UserService) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.logout = function(){
        UserService.logout.query(function(data){
            console.log("inside GlobalCtrl.logout()");
            $rootScope.globalUser = {};
            $location.path('/login');
        }, function(error){
            console.log(error);
        });
    }

    UserService.getLoggedinUser.query(function(data){
      $rootScope.globalUser = data;
      UserService.user = data;
      /*$location.path('/profile');*/
    }, function(error){
        $scope.loginError = "wrong credentials";
    });

    // toggle error message alert box
    $rootScope.hideAlertBox = function(){
        $rootScope.showAlert = false;
    }


  });
