'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:DeletePropertyCtrl
 * @description
 * # DeletePropertyCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('DeletePropertyCtrl', function ($scope, $location, $rootScope, $timeout, AcePropManagerFactory) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.keysToDelete = [];
    $scope.propertyKeys = [];

    AcePropManagerFactory.getPropertiesByUser.query(function(data){
        $scope.propertyKeys = data.keys;
    }, function(error){
        $rootScope.alertMessage = error.data;
        $rootScope.showAlert = true;
        $rootScope.alertClass = 'alert alert-dismissable alert-danger';
        $timeout($rootScope.hideAlertBox, 4000);
    });

    $scope.toggleSelection = function toggleSelection(key) {
        var index = $scope.keysToDelete.indexOf(key);
        if(index > -1){
            $scope.keysToDelete.splice(index,1);
        }else{
            $scope.keysToDelete.push(key);
        }
        console.log($scope.keysToDelete);
    };

    $scope.deleteKeys = function(){
        var keysToSend = {"keys":$scope.keysToDelete};
        AcePropManagerFactory.deleteKeys.query(keysToSend, function(data){
            console.log("deteled keys");
            for(var key in $scope.keysToDelete){
                var index = $scope.propertyKeys.indexOf($scope.keysToDelete[key]);
                if(index > -1){
                    $scope.propertyKeys.splice(index,1);
                }
            }
        }, function(error){
            console.log("something went wrong");
        });
    };
  });

