'use strict';

/**
 * @ngdoc function
 * @name acePropManagerApp.controller:PropertiesCtrl
 * @description
 * # PropertiesCtrl
 * Controller of the acePropManagerApp
 */
angular.module('acePropManagerApp')
  .controller('PropertiesCtrl', function ($scope, $rootScope, $timeout, AcePropManagerFactory) {

    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

    $scope.currentPage = 0;
    $scope.pageSize = 100;
    $scope.isEditModeOn = false;
    $scope.editMode = [];
    $scope.editedProperties = {};
    $scope.properties = [];
    $scope.shownProperties = [];
    $scope.reverse = true;
    $scope.hideGeLan = true;

    AcePropManagerFactory.getPropertiesListWithDetails.query(
        function(data){
            /*console.log('properties list is ' + JSON.stringify(data));*/
            /*angular.copy(data.toJSON(), $scope.propertiesObj);*/
            $scope.propertiesObj = data.toJSON();
            for(var key in $scope.propertiesObj){
                $scope.properties.push($scope.propertiesObj[key]);
            }
            $scope.shownProperties = $scope.properties;
            $rootScope.properties = $scope.properties;
            for (var i = 0; i < $scope.properties.length; i++){
                $scope.editMode[i] = 0;
            }
        },
        function(error){
            console.log('Ouch something went wrong ' + error.data);
        }
    );

    /*$scope.changeMode = function($index){
        $scope.editMode[$index] = 1;
    }*/

    $scope.onBlur = function(property, language){
        $scope.editMode = [];
        $scope.editedProperties[property.property_key] = property;
        console.log("edited property is " + JSON.stringify($scope.editedProperties));
    }

    $scope.reverseSortOrder = function(){
        $scope.reverse = !$scope.reverse;
    }

    $scope.getHeader = function () {
        return ["property_key", "value_en", "value_th", "value_ar", "value_ru","value_da","value_nl","value_fi","value_fr","value_de","value_el","value_it","value_nb","value_sv","value_tr","file_name"]
    };

    $scope.prepareProperties = function(filteredProperties){
        // Uncomment the lines in this function to have download function work in Safari browser
        var usedProperties = [];
        /*var csvContent = "data:text/csv;charset=utf-8,";*/
        for(var i=0; i<filteredProperties.length; i++){
            var property = filteredProperties[i];
            var usedProperty = {};
            usedProperty.property_key = property.property_key;
            /*if(property.value_en != $scope.propertiesObj[property_key].value_en){
                usedProperty.value_en = property.value_en;    
            }
            if(property.value_th != $scope.propertiesObj[property_key].value_th){
                usedProperty.value_th = property.value_th;    
            }
            if(property.value_ru != $scope.propertiesObj[property_key].value_ru){
                usedProperty.value_ru = property.value_ru;    
            }*/
            usedProperty.value_en = property.value_en;
            usedProperty.value_th = property.value_th;
            usedProperty.value_ar = property.value_ar;
            usedProperty.value_ru = property.value_ru;
            usedProperty.value_da = property.value_da;
            usedProperty.value_nl = property.value_nl;
            usedProperty.value_fi = property.value_fi;
            usedProperty.value_fr = property.value_fr;
            usedProperty.value_de = property.value_de;
            usedProperty.value_el = property.value_el;
            usedProperty.value_it = property.value_it;
            usedProperty.value_nb = property.value_nb;
            usedProperty.value_sv = property.value_sv;
            usedProperty.value_tr = property.value_tr;
            usedProperty.file_name = property.file_name;
            usedProperties.push(usedProperty);
            /*var dataString = '"' + property.property_key + '","' + property.value_en + '","' + property.value_th + '","' + property.value_ge + '"' + '\n';
            csvContent += dataString;*/
        }
        /*var encodedUri = encodeURI(csvContent);
        window.open(encodedUri);*/
        return usedProperties;
    }


    $scope.openFrom = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.fromOpened = true;
    };
    $scope.openTo = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.toOpened = true;
    };

    $scope.onDateChanged = function(fromDate, toDate){
        console.log("from date is " + fromDate);
        console.log("to date is " + toDate);
        if(angular.isUndefined(fromDate) || angular.isUndefined(toDate)
            || fromDate === null || toDate === null){
            $scope.shownProperties = $scope.properties;
        }else{
            var dateFilteredProperties = [];
            for(var i = 0; i < ($scope.properties).length; i++){
                var modified_on = new Date($scope.properties[i].modified_on);
                if(modified_on >= fromDate && modified_on <= toDate){
                    dateFilteredProperties.push($scope.properties[i]);
                }
            }
            $scope.shownProperties = dateFilteredProperties;
            console.log("length = " + dateFilteredProperties.length);  
        }
    }

    $scope.saveEdits = function(){
        console.log("edited properties are " + JSON.stringify($scope.editedProperties));
        AcePropManagerFactory.updateProperties.query($scope.editedProperties, 
            function(data){
                console.log("updated successfully");
                $rootScope.alertMessage = "successfully edited";
                $rootScope.showAlert = true;
                $rootScope.alertClass = 'alert alert-dismissable alert-success';
                $timeout($rootScope.hideAlertBox, 4000);
            }, 
            function(error){

            }
        );
    }

});
