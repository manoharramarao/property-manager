'use strict';

/**
 * @ngdoc service
 * @name acePropManagerApp.UserService
 * @description
 * # UserService
 * Factory in the acePropManagerApp.
 */
angular.module('acePropManagerApp')
  .factory('UserService', ['$resource', '$location', function ($resource, $location) {
    /*var baseUrl = "http://localhost:8000/aceappmanager/";*/
    /*var baseUrl = "http://hoeapp354.na.xom.com:8000/aceappmanager/";*/
    var baseUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/" + "aceappmanager/";
    var user = {};
    
    return {
      login: $resource(baseUrl + 'user/login.json', {}, {
        query: {
          method: 'POST',
          params: {},
          headers: {'Content-Type': 'application/json; charset=UTF-8'},
          isArray: false
        }
      }),
      logout: $resource(baseUrl + 'user/logout.json', {}, {
        query: {
          method: 'POST',
          params: {},
          isArray: false
        }
      }),
      getLoggedinUser: $resource(baseUrl + 'user/get_loggedin_user.json', {}, {
        query: {
          method: 'POST',
          params: {},
          isArray: false
        }
      }),
      updateProperties: $resource(baseUrl + 'user/update_properties.json', {}, {
        query: {
          method: 'POST',
          params: {},
          isArray: false
        }
      }),
    };
  }]);
