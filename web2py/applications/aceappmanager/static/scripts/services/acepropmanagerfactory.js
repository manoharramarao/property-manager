'use strict';



/**
 * @ngdoc service
 * @name acePropManagerApp.AcePropManagerFactory
 * @description
 * # AcePropManagerFactory
 * Factory in the acePropManagerApp.
 */
angular.module('acePropManagerApp')
  .factory('AcePropManagerFactory', ['$resource', '$location', function ($resource, $location) {
    /*var baseUrl = "http://localhost:8000/aceappmanager/";*/
    /*var baseUrl = "http://hoeapp354.na.xom.com:8000/aceappmanager/";*/
    var baseUrl = $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/" + "aceappmanager/";
    
    // Public API here
    return {
      getPropertiesListWithDetails: $resource(baseUrl + 'v1/get_properties.json', {}, {
        query: {
          method: 'GET',
          params: {},
          headers: {'Content-Type': 'application/json; charset=UTF-8'},
          isArray: false
        }
      }),
      addProperties: $resource(baseUrl + 'user/add_properties.json', {}, {
        query: {
          method: 'POST',
          params: {},
          headers: {'Content-Type': 'application/json; charset=UTF-8'},
          isArray: false
        }
      }),
      updateProperties: $resource(baseUrl + 'user/update_properties.json', {}, {
        query: {
          method: 'POST',
          params: {},
          isArray: false
        }
      }),
      getPropFileNames: $resource(baseUrl + 'user/get_prop_file_names.json', {}, {
        query: {
          method: 'POST',
          params: {},
          isArray: true
        }
      }),
      getPropertiesByUser: $resource(baseUrl + 'user/get_properties_by_user.json', {}, {
        query: {
          method: 'POST',
          params: {},
          isArray: false
        }
      }),
      deleteKeys: $resource(baseUrl + 'user/delete_keys_by_user.json', {}, {
        query: {
          method: 'POST',
          params: {},
          isArray: false
        }
      }),
      saveArticle: $resource(baseUrl + 'wiki/save_article.json', {}, {
        query: {
            method: 'POST',
            params: {},
            isArray: false
        }
      }),
    }
  }]);
