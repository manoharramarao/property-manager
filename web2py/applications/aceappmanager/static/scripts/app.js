'use strict';

/**
 * @ngdoc overview
 * @name acePropManagerApp
 * @description
 * # acePropManagerApp
 *
 * Main module of the application.
 */
angular
  .module('acePropManagerApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularFileUpload',
    'ngCsv',
    'ui.bootstrap',
    'textAngular',
  ])
  .config(function ($routeProvider, $httpProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/show', {
        templateUrl: 'views/properties_list.html',
        controller: 'PropertiesCtrl'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .when('/import', {
        templateUrl: 'views/import.html',
        controller: 'ImportCtrl'
      })
      .when('/add_property', {
        templateUrl: 'views/add_property.html',
        controller: 'AddPropertyCtrl'
      })
      .when('/profile', {
        templateUrl: 'views/profile.html',
        controller: 'ProfileCtrl'
      })
      .when('/download', {
        templateUrl: 'views/download.html',
        controller: 'DownloadCtrl'
      })
      .when('/styleguide', {
        templateUrl:'/styleguide.html'
      })
      .when('/delete', {
        templateUrl: 'views/delete_property.html',
        controller: 'DeletePropertyCtrl'
      })
      .when('/wiki',{
        templateUrl: 'views/wiki.html',
        controller: 'WikiCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });

    $httpProvider.defaults.withCredentials = true;
  });
