'use strict';

/**
 * @ngdoc filter
 * @name acePropManagerApp.filter:startFrom
 * @function
 * @description
 * # startFrom
 * Filter in the acePropManagerApp.
 */
angular.module('acePropManagerApp')
  .filter('startFrom', function () {
    return function (input, start) {
        start = +start;
        return input.slice(start);
    };
  });
