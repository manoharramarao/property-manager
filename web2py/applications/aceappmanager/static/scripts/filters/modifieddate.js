'use strict';

/**
 * @ngdoc filter
 * @name acePropManagerApp.filter:ModifiedDate
 * @function
 * @description
 * # ModifiedDate
 * Filter in the acePropManagerApp.
 */
angular.module('acePropManagerApp')
  .filter('ModifiedDate', function () {
    return function (input, fromDate, toDate) {
        if(!angular.isUndefined(fromDate) && !angular.isUndefined(toDate)){
            var dateFilteredProperties = [];
            for(var i = 0; i < input.length; i++){
                var modified_on = new Date(input[i].modified_on);
                if(modified_on >= fromDate && modified_on <= toDate){
                    dateFilteredProperties.push(input[i]);
                }
            }
            console.log("length = " + dateFilteredProperties.length);  
        }
        return 'ModifiedDate filter: ' + dateFilteredProperties;

    };
  });
