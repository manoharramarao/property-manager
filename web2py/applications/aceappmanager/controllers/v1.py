# -*- coding: utf-8 -*-

import logging
import os
import gluon.contenttype
import zipfile

logger = logging.getLogger("aceappmanager.v1")
logger.setLevel(logging.DEBUG)

@request.restful()
def api():
    logger.debug("inside api")
    response.view = 'generic.'+request.extension
    def GET(*args,**vars):
        patterns = [
            "/key[property]",
            "/key/{property.property_key.startswith}",
            "/key/id/{property.id}",
            "/key/id/{property.id}/:field",
            ]
        parser = db.parse_as_rest(patterns,args,vars)
        if parser.status == 200:
            return dict(content=parser.response)
        else:
            raise HTTP(parser.status,parser.error)
    def POST(*args, **vars):
        return dict()
    def DELETE(*args, **vars):
        return dict()
    def PUT(*args, **vars):
        return dict()
    return locals()


def get_properties():
    logger.debug("inside get_properties")
    rows = db(db.property.is_active==True).select(db.property.ALL)
    for row in rows:
        row.update(id=row.property_key)
    return rows.as_dict()
    # return rows.as_json()


def my_big_file_downloader():
    """
    """
    import os
    filename = request.args[0]
    pathfilename = os.path.join(request.folder, 'uploads/', filename)
    return response.stream(open(pathfilename, 'rb'), chunk_size=4096)

def my_small_file_downloader():
    generate_properties_file()
    filename=request.args[0]
    response.headers['Content-Type']=gluon.contenttype.contenttype(filename)
    pathfilename=os.path.join(request.folder,'uploads/', filename)
    return open(pathfilename, 'rb').read()

def generate_properties_file():
    print "inside generate_properties_file"
    filename_filehandler = dict()
    pathfilename = os.path.join(request.folder, 'uploads/')
    for row in db().select(db.files.ALL):
        print "file name is " + row.filename;
        if "base" == row.filename:
            filename_filehandler[row.filename + '.properties'] = open(pathfilename + "/files/" + row.filename + '.properties', 'w')
        filename_filehandler[row.filename + '_en.properties'] = open(pathfilename + "/files/" + row.filename + '_en.properties', 'w')
        filename_filehandler[row.filename + '_th.properties'] = open(pathfilename + "/files/" + row.filename + '_th.properties', 'w')
        filename_filehandler[row.filename + '_ar.properties'] = open(pathfilename + "/files/" + row.filename + '_ar.properties', 'w')
        filename_filehandler[row.filename + '_ru.properties'] = open(pathfilename + "/files/" + row.filename + '_ru.properties', 'w')
        filename_filehandler[row.filename + '_da.properties'] = open(pathfilename + "/files/" + row.filename + '_da.properties', 'w')
        filename_filehandler[row.filename + '_nl.properties'] = open(pathfilename + "/files/" + row.filename + '_nl.properties', 'w')
        filename_filehandler[row.filename + '_fi.properties'] = open(pathfilename + "/files/" + row.filename + '_fi.properties', 'w')
        filename_filehandler[row.filename + '_fr.properties'] = open(pathfilename + "/files/" + row.filename + '_fr.properties', 'w')
        filename_filehandler[row.filename + '_de.properties'] = open(pathfilename + "/files/" + row.filename + '_de.properties', 'w')
        filename_filehandler[row.filename + '_el.properties'] = open(pathfilename + "/files/" + row.filename + '_el.properties', 'w')
        filename_filehandler[row.filename + '_it.properties'] = open(pathfilename + "/files/" + row.filename + '_it.properties', 'w')
        filename_filehandler[row.filename + '_nb.properties'] = open(pathfilename + "/files/" + row.filename + '_nb.properties', 'w')
        filename_filehandler[row.filename + '_sv.properties'] = open(pathfilename + "/files/" + row.filename + '_sv.properties', 'w')
        filename_filehandler[row.filename + '_tr.properties'] = open(pathfilename + "/files/" + row.filename + '_tr.properties', 'w')
    # output_file_en = open('/tmp/myfile_en.properties', 'w')
    # output_file_th = open('/tmp/myfile_th.properties', 'w')
    for row in db().select(db.property.ALL):
        if row.file_name is not None:
            if row.value_en is not None and "" != row.value_en:
                filename_filehandler[row.file_name + '_en.properties'].write(row.property_key + "=" + row.value_en + "\n")
                if "base" == row.file_name:
                    filename_filehandler[row.file_name + '.properties'].write(row.property_key + "=" + row.value_en + "\n")
            if row.value_th is not None and "" != row.value_th:
                filename_filehandler[row.file_name + '_th.properties'].write(row.property_key + "=" + row.value_th + "\n")
            if row.value_ar is not None and "" != row.value_ar:
                filename_filehandler[row.file_name + '_ar.properties'].write(row.property_key + "=" + row.value_ar + "\n")
            if row.value_ru is not None and "" != row.value_ru:
                filename_filehandler[row.file_name + '_ru.properties'].write(row.property_key + "=" + row.value_ru + "\n")
            if row.value_da is not None and "" != row.value_da:
                filename_filehandler[row.file_name + '_da.properties'].write(row.property_key + "=" + row.value_da + "\n")
            if row.value_nl is not None and "" != row.value_nl:
                filename_filehandler[row.file_name + '_nl.properties'].write(row.property_key + "=" + row.value_nl + "\n")
            if row.value_fi is not None and "" != row.value_fi:
                filename_filehandler[row.file_name + '_fi.properties'].write(row.property_key + "=" + row.value_fi + "\n")
            if row.value_fr is not None and "" != row.value_fr:
                filename_filehandler[row.file_name + '_fr.properties'].write(row.property_key + "=" + row.value_fr + "\n")
            if row.value_de is not None and "" != row.value_de:
                filename_filehandler[row.file_name + '_de.properties'].write(row.property_key + "=" + row.value_de + "\n")
            if row.value_el is not None and "" != row.value_el:
                filename_filehandler[row.file_name + '_el.properties'].write(row.property_key + "=" + row.value_el + "\n")
            if row.value_it is not None and "" != row.value_it:
                filename_filehandler[row.file_name + '_it.properties'].write(row.property_key + "=" + row.value_it + "\n")
            if row.value_nb is not None and "" != row.value_nb:
                filename_filehandler[row.file_name + '_nb.properties'].write(row.property_key + "=" + row.value_nb + "\n")
            if row.value_sv is not None and "" != row.value_sv:
                filename_filehandler[row.file_name + '_sv.properties'].write(row.property_key + "=" + row.value_sv + "\n")
            if row.value_tr is not None and "" != row.value_tr:
                filename_filehandler[row.file_name + '_tr.properties'].write(row.property_key + "=" + row.value_tr + "\n")
    for row in db().select(db.files.ALL):
        if "base" == row.filename:
            filename_filehandler[row.filename + '.properties'].close()
        filename_filehandler[row.filename + '_en.properties'].close()
        filename_filehandler[row.filename + '_th.properties'].close()
        filename_filehandler[row.filename + '_ar.properties'].close()
        filename_filehandler[row.filename + '_ru.properties'].close()
        filename_filehandler[row.filename + '_da.properties'].close()
        filename_filehandler[row.filename + '_nl.properties'].close()
        filename_filehandler[row.filename + '_fi.properties'].close()
        filename_filehandler[row.filename + '_fr.properties'].close()
        filename_filehandler[row.filename + '_de.properties'].close()
        filename_filehandler[row.filename + '_el.properties'].close()
        filename_filehandler[row.filename + '_it.properties'].close()
        filename_filehandler[row.filename + '_nb.properties'].close()
        filename_filehandler[row.filename + '_sv.properties'].close()
        filename_filehandler[row.filename + '_tr.properties'].close()
    zipf = zipfile.ZipFile(pathfilename + '/properties.zip', 'w')
    zipdir(pathfilename + "/files/", zipf)
    zipf.close()


def zipdir(path, zip):
    for root, dirs, files in os.walk(path):
        for file in files:
            zip.write(os.path.join(root, file), os.path.basename(file))


def make_all_active():
    db(db.property.property_key == "Testing").update(is_active=(not False))
    return
