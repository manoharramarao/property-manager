# -*- coding: utf-8 -*-

from gluon.storage import Storage
import gluon.contrib.simplejson as json
import logging
import os
import shutil
import csv
import traceback

logger = logging.getLogger("aceappmanager.user")
logger.setLevel(logging.DEBUG)

if False:
    from gluon import *
    request = current.request
    response = current.response
    session = current.response
    cache = current.cache
    db = current.db
    auth = current.auth
    logger = current.logger
    T = current.T


def logout():
    if request.env.request_method != "OPTIONS":
        try:
            session.auth = None
            result = {"msg": T('successfully_logged_out')}
        except Exception, e:
            traceback.print_exc()
            raise HTTP(500, T('http_500_error'))
        return result


def login():
    try:
        input_json = request.body.read()
        user = Storage(json.loads(input_json))
        user.password = user.password.encode("utf-8")
        user = auth.login_bare(user.email, user.password)
        user.password = None
        return user.as_dict()
    except InputValidationError as e:
        raise HTTP(422, e.msg)
    except AuthenticationError as e:
        traceback.print_exc()
        raise HTTP(422, e.msg)
    except Exception as e:
        traceback.print_exc()
        raise HTTP(500, T('http_500_error'))


# def download_properties():
#     import cStringIO
#     stream = cStringIO.StringIO()
#     properties = db().select(db.property.ALL)
#     print properties
#     properties.export_to_csv_file(stream)
#     response.headers['Content-Type'] = "application/vnd.ms-excel"
#     response.write(stream.getvalue(), escape=False)


def upload_properties():
    if request.env.request_method != "OPTIONS":
        fsrc = request.vars.file.file
        db['property'].import_from_csv_file(fsrc, unique="property_key")
        result = dict()
        result["result"] = "success"
        return "success"


def add_properties():
    logger.debug("inside add_properties")
    if auth.is_logged_in():
        input_json = request.body.read()
        propertiesInput = Storage(json.loads(input_json))
        property_to_insert = Storage()
        properties = propertiesInput.properties.split(',')
        query1 = db.property.property_key.belongs(properties)
        # query2 = db.property.file_name == property_to_insert.file_name
        if db(query1).select():
            raise HTTP(500, T("Key already exists"))
        else:
            for property in properties:
                logger.debug("property is " + property)
                property_to_insert.artifact_id = propertiesInput.artifactId
                property_to_insert.proj_release = propertiesInput.release
                property_to_insert.sprint = propertiesInput.sprint
                property_to_insert.file_name = propertiesInput.fileName
                property_to_insert.created_by_id = auth.user.email
                property_to_insert.updated_by_id = auth.user.email
                property_to_insert.property_key = property.strip()
                return_result = db['property'].insert(**property_to_insert)
                logger.debug(return_result)
        raise HTTP(200, T("Successfully added"))
    return


def update_properties():
    logger.debug("inside update_properties")
    result="fail"
    if auth.is_logged_in():
        logger.debug(str(auth.user_groups))
        for group in auth.user_groups.itervalues():
            if group == "business" or group == "developer" or group == "admin":
                input_json = request.body.read()
                properties_updated = Storage(json.loads(input_json))
                logger.debug("updated properties are " + str(properties_updated))
                count = 0
                for ind_property in properties_updated.values():
                    ind_property['updated_by_id'] = auth.user.email
                    ind_property['modified_on'] = request.utcnow
                    db['property'].update_or_insert(db.property.property_key==ind_property['property_key'], **ind_property)
                result = "success"
                break
        if result == "success":
            return {"result": "success"}
        else:
            raise HTTP(401, T('you are not allowed to update properties'))

def get_loggedin_user():
    try:
        if session.auth.user:
            result = dict(session.auth.user)
        else:
            return
    except Exception, e:
        traceback.print_exc()
        raise HTTP(500, T('http_500_error'))
    return result


def get_prop_file_names():
    logger.debug("inside get_prop_file_names");
    rows=db().select(db.files.filename)
    return rows.as_json()


def get_properties_by_user():
    logger.debug("inside get_properties_by_user")
    if not session or not session.auth or not session.auth.user:
        raise HTTP(403, T("Please log in"))
    user = session.auth.user
    query = db.property.created_by_id == user.email
    rows = db(query).select(db.property.property_key)
    keys = list()
    for row in rows:
        keys.append(row.property_key)
    return {"keys": keys}


def delete_keys_by_user():
    logger.debug("inside delete_keys_by_user")
    if not session.auth.user:
        raise HTTP(403, T('You are not allowed to delete these properties'))
    input_json = request.body.read()
    keys_to_delete = json.loads(input_json)
    query1 = db.property.created_by_id == session.auth.user.email
    query2 = db.property.property_key.belongs(keys_to_delete["keys"])
    db(query1 & query2).delete()
    return {"result": T("keys deleted successfully")}
