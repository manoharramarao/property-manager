# -*- coding: utf-8 -*-
from datetime import datetime


print "utcnow = " + str(request.utcnow)


db.define_table(
    'property',
    Field('property_key', 'string', required=True, notnull=True, label='Key', unique=True),
    Field('value_en', 'string'),
    Field('value_th', 'string'),
    Field('value_ar', 'string'),
    Field('value_ru', 'string'),
    Field('value_da', 'string'),
    Field('value_nl', 'string'),
    Field('value_fi', 'string'),
    Field('value_fr', 'string'),
    Field('value_de', 'string'),
    Field('value_el', 'string'),
    Field('value_it', 'string'),
    Field('value_nb', 'string'),
    Field('value_sv', 'string'),
    Field('value_tr', 'string'),
    Field('file_name', 'string', required=True, label='File name'),
    Field('created_on', 'datetime', default=request.utcnow),
    Field('modified_on', 'datetime', update=request.utcnow, default=request.utcnow),
    Field('created_by_id', 'string'),
    Field('updated_by_id', 'string'),
    Field('artifact_id', 'string'),
    Field('proj_release', 'integer'),
    Field('sprint', 'integer'),
    Field('is_active', 'boolean', writable=False, readable=False, default=True),
    format='%(property_key)s'
)

db.property._enable_record_versioning(
    archive_db=db,
    archive_name='property_archive',
    current_record='current_record',
    is_active='is_active')

db.define_table(
    'files',
    Field('filename', 'string', required=True, unique=True, label='File name')
)

db.define_table(
    'article',
    Field('notes', 'text', required=True, label='Notes'),
    Field('article_desc', 'string', required=True, label='Description'),
    Field('created_on', 'datetime', default=request.utcnow),
    Field('modified_on', 'datetime', update=request.utcnow, default=request.utcnow),
    Field('created_by_id', 'string'),
    Field('modified_by_id', 'string'),
    Field('is_active', 'boolean', writable=False, readable=False, default=True),
)

db.article._enable_record_versioning(
    archive_db=db,
    archive_name='article_archive',
    current_record='current_record',
    is_active='is_active')


# response.headers['Access-Control-Allow-Methods'] = ['GET', 'POST', 'OPTIONS']
# response.headers['Access-Control-Allow-Origin'] = "http://localhost:9000"
# response.headers['Access-Control-Allow-Origin'] = "http://hoeapp354.na.xom.com:9012"
# response.headers['Access-Control-Allow-Headers'] = ['Content-Type']
# response.headers['Access-Control-Allow-Credentials'] = 'true'